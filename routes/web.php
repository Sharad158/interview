<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



    Route::get('/product', [App\Http\Controllers\ProductController::class, 'show'])->name('show')->middleware('auth');
    Route::get('product_delete/{id}',[App\Http\Controllers\ProductController::class, 'destroy'])->name('delete')->middleware('auth');
    Route::get('/product_create', [App\Http\Controllers\ProductController::class, 'create'])->name('create')->middleware('auth');
    Route::POST('/product_submit', [App\Http\Controllers\ProductController::class, 'store'])->name('submit')->middleware('auth');
    
    Route::get('product_edit/{id}',[App\Http\Controllers\ProductController::class, 'edit'])->name('edit')->middleware('auth');
    Route::POST('product_update/{id}',[App\Http\Controllers\ProductController::class, 'update'])->name('update')->middleware('auth');


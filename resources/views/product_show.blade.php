@extends('layouts.layout')

@section('content')

<body class="antialiased">
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center sm:pt-0">

        <div class="form-group" style="margin:10px;">
            <a href="{{route('create')}}" type="button" class="btn btn-primary">Add New Products</a>
            <a href="{{route('home')}}" type="button" class="btn btn-primary">Back To Home Page</a>
        </div>
        
<br>
        @if(Session::has('msg'))
        <p class="alert alert-info">{{ session('msg') }}</p>
        @endif

    <table id="customers">
        <tr>
            <th>SKU</th>
            <th>Name</th>
            <th>Price</th>
            <th>Category</th>
            <th>Quantity</th>
            <th>Image</th>
            <th>Action</th>
        </tr>
       
       
        @if(count($productarr) > 0)
            @foreach($productarr as $data)
            @php
            $new =  explode("/",$data->Image);
            // print_r($new[1]) ;            
         @endphp
            <tr>
                <td>{{ $data->SKU }}</td>
                <td>{{ $data->Name }}</td>
                <td>{{ $data->Price }}</td>
                <td>{{ $data->Category }}</td>
                <td>{{ $data->Quantity }}</td>
                <td><img src="{{asset('storage').'/'.$new[1]}}" width="100px"></td>               
                <td>
                    <a href="{{route('delete',$data->id)}}">Delete</a>
                    <a href="{{route('edit',$data->id)}}">Edit</a>
                </td>
            </tr>
            @endforeach
            @else 
            <tr>
            <td colspan="7" class="text-center"> <strong>Sorry!</strong> No Product Found. </td>
            </tr>
        @endif
       
    </table>
       
    </div>
</body>
@endsection


